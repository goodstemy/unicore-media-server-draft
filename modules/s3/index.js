const AWS = require('aws-sdk');

const s3 = new AWS.S3({
    accessKeyId: process.env.S3_ACCOUNT_ID || '',
    secretAccessKey: process.env.S3_ACCOUNT_SECRET || '',
});

const BUCKET_NAME = process.env.S3_BUCKET_NAME || '';


const methods = {
    getUploadURL: ({
        companyId,
        taskId,
        fileName,
    }) => {
        s3.createPresignedPost({
            Bucket: BUCKET_NAME,
            Conditions: [["content-length-range", 100, 10000000]],
            Fields: {
                key: `${companyId}/${taskId}/${fileName}`,
            },
            Expires: 60,
        }, (err, data) => {
            if (err) {
                console.error('Presigning post data encountered an error', err);
            } else {
                data.fields.key = `${fileName}`;
                console.log('The post data is', data);

                res.send({
                    uploadURL: data.url,
                });
            }
        });
    },
};

module.exports = methods;
