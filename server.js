const express = require('express');
const formidableMiddleware = require('express-formidable');

const AWS = require('aws-sdk');

require('dotenv').config();

const s3 = new AWS.S3({
    accessKeyId: process.env.S3_ACCOUNT_ID || '',
    secretAccessKey: process.env.S3_ACCOUNT_SECRET || '',
    region: 'eu-central-1',
});

const BUCKET_NAME = process.env.S3_BUCKET_NAME || '';

const {
    getUploadURL,
} = require('./modules/s3');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/api/v1/get-upload-url', async (req, res) => {
    const {fileName} = req.body;

    const uploadURL = await getUploadURL(fileName);

});

app.post('/api/v1/get-object-size', (req, res) => {
    const {key} = req.body;

    s3.headObject({
        Key: key,
        Bucket: BUCKET_NAME,
    }, (err, data) => {
        if (err) {
            return console.error('Error: ', err);
        }

        res.send(data);
    });
});

app.post('/api/v1/put', (req, res) => {
    const {files} = req;

    for (const [objectName, file] of Object.entries(files)) {
        const {name: fileName} = file;
        const [taskId, mediaRefName] = objectName.split('_');



    }
});

app.listen(process.env.PORT || 3000);
